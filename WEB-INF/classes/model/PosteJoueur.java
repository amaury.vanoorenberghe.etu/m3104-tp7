package model;

public enum PosteJoueur {
	GAR,
	DEF,
	ATT;
	
	public static final PosteJoueur fromName(String name) {
		if (name == null || name.length() == 0) {
			return ATT;
		}
		final PosteJoueur result = valueOf(name.toUpperCase());
		if (result == null) {
			return ATT;
		}
		return result;
	}
	
	public static final String getName(String name) {
		return fromName(name).name();
	}
}
