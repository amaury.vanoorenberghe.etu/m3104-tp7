package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@SuppressWarnings("serial")
@WebServlet("/" + ChoisirJoueur.SERVLET_PUBLIC_NAME)
public class ChoisirJoueur extends HttpServlet {
	public static final String SERVLET_PUBLIC_NAME = "ChoisirJoueur";
	
	protected static final String[] DROP_PARAMS = {"id"};
	protected static final List<String> DROP = Arrays.asList(DROP_PARAMS);
	
	public static final String PARAM_ID = "id";
	public static final String SESSION_TEAM = ListerJoueur.SESSION_TEAM;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		final HttpSession session = req.getSession(true);
		
		final String paramId = req.getParameter(PARAM_ID);
		
		if (paramId == null) {
			redirect(req, resp);
			return;
		}
		
		final int id = Integer.parseInt(paramId);
		
		if (session.getAttribute(SESSION_TEAM) == null) {
			session.setAttribute(SESSION_TEAM, new ArrayList<Integer>());
		}
		
		List<Integer> team = (List<Integer>)session.getAttribute(SESSION_TEAM);
		team.add(id);
		
		redirect(req, resp);
	}
	
	private void redirect(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		final String params = "?" + req
		.getParameterMap()
		.entrySet()
		.stream()
		.filter(item -> !DROP.contains(item.getKey()))
		.map(item -> String.format("%s=%s", item.getKey().toString(), 
			Arrays
			.stream(item.getValue())
			.collect(Collectors.joining(","))
		))
		.collect(Collectors.joining("&"));
		resp.sendRedirect(ListerJoueur.SERVLET_PUBLIC_NAME + params);
	}
}
