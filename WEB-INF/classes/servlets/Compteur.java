package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import util.ContentType;

@SuppressWarnings("serial")
@WebServlet("/" + Compteur.SERVLET_PUBLIC_NAME)
public class Compteur extends HttpServlet {
	public static final String SERVLET_PUBLIC_NAME = "Compteur";
	protected static final String COOKIE_VISIT_COUNT = "visits";
	
	private int nbVisitors = 0;
	
	@SuppressWarnings("deprecation")
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		final HttpSession session = req.getSession(true);
		
		++nbVisitors;
		
		Integer nbVisitsAttr = (Integer)session.getAttribute(COOKIE_VISIT_COUNT);
		int nbVisits = 1 + new Integer(nbVisitsAttr == null ? 0 : nbVisitsAttr.intValue());
		session.setAttribute(COOKIE_VISIT_COUNT, nbVisits);
		
		resp.setContentType(ContentType.TEXT_HTML_UTF_8);
		
		PrintWriter out = resp.getWriter();
		
		out.println("<!DOCTYPE html>");
		out.println("<html lang=\"fr\">");
		
		htmlHead(out);
		
		htmlBody(nbVisits, out);
		
		out.println("</html>");
	}

	private void htmlBody(int nbVisits, PrintWriter out) {
		out.println("<body>");
		out.println(String.format("<style>.rotate {transform: rotate(%ddeg)}</style>", nbVisits - 1));
		out.println("<div class=\"container\">");
		out.println("<div class=\"counter\">");
		out.println("<div class=\"tile rotate\">");
		out.println(String.format("<span>%d &mdash; %d</span>", nbVisits, nbVisitors));
		out.println("</div>");
		out.println("</div>");
		out.println("</div>");
		out.println("</body>");
	}

	private void htmlHead(PrintWriter out) {
		out.println("<head>");
		out.println("<title>Compteur</title>");
		out.println("<link rel=\"stylesheet\" href=\"css/compteur.css\">");
		out.println("</head>");
	}
}