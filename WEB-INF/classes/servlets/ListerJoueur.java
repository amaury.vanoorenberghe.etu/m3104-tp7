package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.StringJoiner;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import database.SQLProvider;
import model.PosteJoueur;
import util.ContentType;

@SuppressWarnings("serial")
@WebServlet("/" + ListerJoueur.SERVLET_PUBLIC_NAME)
public class ListerJoueur extends HttpServlet {
	public static final String SERVLET_PUBLIC_NAME = "ListerJoueur";
	
	protected static final String SQL_QUERY = "SELECT * FROM joueurs WHERE poste = ? AND num_joueur NOT IN %s ORDER BY nom_joueur ASC";
	
	public static final String SESSION_TEAM = "team";
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType(ContentType.TEXT_HTML_UTF_8);
		
		PrintWriter out = resp.getWriter();
		
		out.println("<!DOCTYPE html>");
		out.println("<html lang=\"fr\">");
		
		htmlHead(out);
		htmlBody(req, resp, out);
		
		out.println("</html>");
	}

	private void htmlBody(HttpServletRequest req, HttpServletResponse resp, PrintWriter out) throws ServletException {
		final String poste = PosteJoueur.getName(req.getParameter("poste"));
		final String team = getTeam(req);
		
		out.println("<body>");
		out.println("<center>");
		out.println("<h1>Liste des joueurs</h1>");

		out.println("<h2>");
		out.println("<a href=\"?poste=GAR\">Gardien</a>");
		out.println("<a href=\"?poste=ATT\">Attaquant</a>");
		out.println("<a href=\"?poste=DEF\">Défenseur</a>");
		out.println("</h2>");
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet results = null;
		try {
			connection = SQLProvider.instance().getConnection();
			statement = connection.prepareStatement(String.format(SQL_QUERY, team));
			statement.setString(1, poste);
			results = statement.executeQuery();
			
			
			out.println("<table>");
			out.println("<tr>");
			out.println("<th>Nom</th>");
			out.println("<th>Poste</th>");
			out.println("<th>&mdash;</th>");
			out.println("</tr>");
			
			while(results.next()) {
				final String idJ = results.getString("num_joueur");
				final String nomJ = results.getString("nom_joueur");
				final String posteJ = results.getString("poste");
				
				out.println("<tr>");
				out.println(String.format("<td>%s</td>", nomJ));
				out.println(String.format("<td>%s</td>", posteJ));
				out.println(String.format("<td><a href=\"ChoisirJoueur?id=%s&poste=%s\">Choisir</a></td>", idJ, poste));
				out.println("</tr>");
			}
			
			out.println("</table>");
		} catch (SQLException e) {
			throw new ServletException(e);
		} finally {
			try {
				connection.close();
				statement.close();
				results.close();
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		out.println("</center>");
		out.println("</body>");
	}

	@SuppressWarnings("unchecked")
	private String getTeam(HttpServletRequest req) {
		final HttpSession session = req.getSession(true);

		StringJoiner sj = new StringJoiner(",", "(", ")");
		
		List<Integer> team = (List<Integer>)session.getAttribute(SESSION_TEAM);
		if (session.getAttribute(SESSION_TEAM) != null) {
			for (int id : team) {
				sj.add(Integer.toString(id));
			}
		}
		
		return sj.toString();
	}

	private void htmlHead(PrintWriter out) {
		out.println("<head>");
		out.println("<title>Liste des joueurs</title>");
		out.println("</head>");
	}
}
